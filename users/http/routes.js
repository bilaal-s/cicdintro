const axios = require('axios');

function routes(app) {
  app.get('/pokemon/:name', async (req, res) => {
    await axios(`https://pokeapi.co/api/v2/${req.params.name}`);
    res.end();
  });
}

module.exports = routes;
