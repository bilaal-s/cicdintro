const mongoose = require('mongoose');
const { Schema } = require('mongoose');

function connectDb() {
  mongoose.connect('mongodb://localhost:27017/userDb', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
}
const ProductSchema = Schema({
  name: String,
  skuCode: Number,
  description: String,
});

const Product = mongoose.model('Product', ProductSchema);

const ProductId = {
  type: Schema.Types.ObjectId,
  ref: 'Product',
};

const UserSchema = Schema({
  name: String,
  email: String,
  products: [ProductId],
});

const User = mongoose.model('User', UserSchema);

module.exports.User = User;
module.exports.Product = Product;
module.exports.connectDb = connectDb;
