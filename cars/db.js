const mongoose = require('mongoose');

function connectDb() {
  mongoose.connect('mongodb://localhost:27017/carsDb', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
}

const Car = mongoose.model('Car', {
  make: String,
  model: String,
  engineSize: String,
});

module.exports.Car = Car;
module.exports.connectDb = connectDb;
