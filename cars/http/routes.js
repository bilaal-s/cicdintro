const { Car } = require('../db');

function routes(app) {
  app.post('/cars', async (req, res) => {
    const car = new Car(req.body);
    const carSaveResult = await car.save();
    res.send({
      message: `Successfully saved car to db ${carSaveResult._id}`,
    });
  });
}

module.exports = routes;
