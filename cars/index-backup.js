const express = require('express');

const app = express();
const { MongoClient } = require('mongodb');

app.use(express.json());

async function start() {
  const client = await MongoClient.connect('mongodb://localhost:27017');
  const db = client.db('carsDb');
  const carsCollection = db.collection('cars');

  app.post('/cars', async (req, res) => {
    const { insertedId } = await carsCollection.insertOne(req.body);
    res.send({
      message: `Successfully saved car to db ${insertedId}`,
    });
  });

  app.get('/cars', async (req, res) => {
    const results = await carsCollection.find().toArray();
    res.send(JSON.stringify(results));
  });
}

start().catch(console.log);

app.listen(3001, () => {
  console.log('Server started on port 3000');
});
