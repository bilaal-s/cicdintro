const express = require('express');

const app = express();
const routes = require('./http/routes');
const { connectDb } = require('./db');

app.use(express.json());

connectDb();
routes(app);

app.listen(3001, () => {
  console.log('Server started on port 3001');
});
