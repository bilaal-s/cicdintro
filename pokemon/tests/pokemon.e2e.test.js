const supertest = require('supertest');
const setupServer = require('../http/server');

describe('Pokemon server', () => {
  // Arrange
  const server = setupServer();
  const testServer = supertest(server);

  it('GET /pokemon/:name should return data', async () => {
    // Act
    const response = await testServer.get('/pokemon/ditto');

    // Assert
    expect(response.body).toEqual({
      height: 3,
      weight: 40,
      id: 132,
      name: 'ditto',
    });
    expect(response.status).toEqual(200);
  });
});
