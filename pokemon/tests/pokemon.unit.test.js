const { setupPokemonRequestHandler } = require('../http/routes');

describe('Pokemon find by name controller', () => {
  it('GET /pokemon/:name handles errors thrown by axios', async () => {
    // Arrange
    const fakeAxios = () => {
      throw Error('Response timed out');
    };
    const requestHandler = setupPokemonRequestHandler(fakeAxios);
    const fakeRequest = { params: { name: 'ditto' } };
    const fakeResponse = {
      status: jest.fn(),
      send: jest.fn(),
    };

    // Act
    await requestHandler(fakeRequest, fakeResponse);

    // Assert
    expect(fakeResponse.send).toHaveBeenCalledWith({
      errorMessage: 'Response timed out',
    });
    expect(fakeResponse.status).toHaveBeenCalledWith(500);
  });
});
