const axios = require('axios');

const transformPokemonData = (pokemon) => ({
  height: pokemon.height,
  weight: pokemon.weight,
  id: pokemon.id,
  name: pokemon.name,
});

function setupPokemonRequestHandler(sendRequest) {
  return async (req, res) => {
    try {
      const pokemonResponse = await sendRequest(
        `https://pokeapi.co/api/v2/pokemon/${req.params.name}`
      );
      const { data: pokemon } = pokemonResponse;
      const transformedPokemonData = transformPokemonData(pokemon);
      res.send(transformedPokemonData);
    } catch (error) {
      res.status(500);
      res.send({
        errorMessage: error.message,
      });
    }
  };
}

function routes(app) {
  app.get('/pokemon/:name', setupPokemonRequestHandler(axios));
}

module.exports.setupPokemonRequestHandler = setupPokemonRequestHandler;
module.exports.routes = routes;
