const setupServer = require('./http/server');

const { PORT } = process.env;

const app = setupServer();
app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
